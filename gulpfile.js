var gulp = require('gulp');
var ts = require('gulp-typescript');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var pump = require('pump');
var changed = require('gulp-changed');
var livereload = require('gulp-livereload');
var cache = require('gulp-cached');

// TS

var tsSrc = "ts/**/*.ts";
var tsJsDst = "js";

gulp.task('ts', gulp.series(function(cb) {
    pump([
        gulp.src(tsSrc),
        sourcemaps.init(),
        ts({
            noImplicitAny: true,
            target: "es5"
        }),
        sourcemaps.write('./maps'),
        gulp.dest(tsJsDst),
        livereload()
    ], cb);
}));

// TS End

// CSS

var scssSrc = "scss/**/*.s[ca]ss";
var cssDst = "css";

gulp.task('css', gulp.series(function() {
    return gulp.src(scssSrc)
        // .pipe(changed(cssDst, {extension: '.css'}))
        .pipe(sourcemaps.init())
        .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('maps', {sourceRoot: '../scss/'}))
        .pipe(gulp.dest(cssDst))
    	.pipe(livereload());
}));

// CSS End
// JS

var jsSrc = ["js/**/*.js", '!js/**/*.min.js'];
var jsDst = "js";

gulp.task('js', gulp.series(function(cb) {
    pump([
        gulp.src(jsSrc),
        changed(jsDst, {extension: '.min.js'}),
        sourcemaps.init(),
        uglify(),
        rename({
            suffix: '.min'
        }),
        sourcemaps.write('./maps'),
        gulp.dest(jsDst),
    	livereload()
    ], cb);
}));

// JS End
// HTML and PHP

var htmlSrc = ["**/*.htm", "**/*.html", "**/*.php", "!node_modules/**"];
// var htmlDst = "";

gulp.task('html', gulp.series(function() {
    return gulp.src(htmlSrc)
        .pipe(cache('html'))
        .pipe(livereload());
}));

// HTML and PHP End

gulp.task('watch', gulp.parallel('css', 'ts', function() {
    livereload.listen({basePath: '.'});
    gulp.watch(scssSrc, gulp.series('css'));
    gulp.watch(tsSrc, gulp.series('ts'));
    gulp.watch(htmlSrc, gulp.series('html'));
}));

gulp.task('default', gulp.series('css', 'ts'));
