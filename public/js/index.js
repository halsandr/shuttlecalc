function timeToMin(time) {
    var timeMinArr = time.split(":", 2).map(Number);
    var timeMin = timeMinArr[1] + (timeMinArr[0] * 60);
    return timeMin;
}
function minToHour(mins) {
    var hoursDec = mins / 60.0;
    var hours = Math.floor(hoursDec);
    var mins = Math.round((hoursDec - hours) * 60);
    return ("0" + hours).slice(-2) + ":" + ("0" + mins).slice(-2);
}
function leadingZero(num) {
    return ("0" + num).slice(-2);
}
function AppViewModel() {
    var self = this;
    self.wDays = [
        0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5
    ];
    self.wDaysSel = ko.observable(5);
    self.fullWeekDec = ko.computed(function () {
        return self.wDaysSel() * 7.5;
    });
    self.fullWeekText = ko.computed(function () {
        var hour = Math.floor(self.fullWeekDec());
        var min = 60 * (self.fullWeekDec() - hour);
        return leadingZero(hour) + ":" + leadingZero(min);
    });
    self.soFarHour = ko.observable("30");
    self.soFarMinute = ko.observable("00");
    self.lunch = ko.observable("60");
    self.start = ko.observable("09:00");
    self.twentyFour = true;
    self.finish = ko.computed(function () {
        var remaining = (self.fullWeekDec() * 60) -
            timeToMin(leadingZero(self.soFarHour()) + ":" + leadingZero(self.soFarMinute()));
        var startMin = timeToMin(self.start()) + parseInt(self.lunch());
        var finishMin = remaining + startMin;
        var suffix = "";
        if (self.twentyFour) {
            if (finishMin > 720) {
                finishMin -= 720;
                suffix = "pm";
            }
            else {
                suffix = "am";
            }
        }
        var finishHour = minToHour(finishMin);
        return finishHour + " " + suffix;
    });
}
// @ts-ignore
var myModel = new AppViewModel();
ko.applyBindings(myModel);
// Init Service Worker
if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('sw.js');
    // .then(function() { console.log("Service Worker Registered"); });
}

//# sourceMappingURL=maps/index.js.map
