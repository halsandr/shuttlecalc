self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('shuttlecalc').then(function(cache) {
     return cache.addAll([
        './',
        './js/index.js',
        './js/knockout-3.5.0.js',
        './css/index.css',
        './manifest.json'
     ]);
   })
 );
});

self.addEventListener('fetch', function(event) {
 // console.log(event.request.url);

 event.respondWith(
   caches.match(event.request).then(function(response) {
     return response || fetch(event.request);
   })
 );
});